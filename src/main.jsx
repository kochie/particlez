/**
 * Created by Robert Koch on 12/15/16.
 * https://threejs.org/docs/scenes/geometry-browser.html#TorusKnotBufferGeometry
 */

import React from 'react';
import ReactDOM from 'react-dom';

import Canvas from './components/Canvas';


ReactDOM.render(<Canvas />, document.getElementById('main'));
